import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Class is a GUI for a job application
 *
 * @author Scott Hasserd
 * @version 0.8
 */
public class JobApp implements ActionListener {

    /** Text field length */
    private static final int TEXT_LENGTH = 25;

    /** JButton to write answers to file */
    private JButton save;

    /** JTextField to input user name */
    private JTextField userNameOutput;

    /** JTextField to input last name */
    private JTextField lastNameOutput;

    /** JTextField to input first name */
    private JTextField firstNameOutput;

    /** JButton to write answers to file and close application*/
    private JButton submit;

    /** JButton to exit application */
    private JButton exit;

    /** JCheckBox for Sunday availability */
    private JCheckBox sun;

    /** JCheckBox for Monday availability */
    private JCheckBox mon;

    /** JCheckBox for Tuesday availability */
    private JCheckBox tue;

    /** JCheckBox for Wednesday availability */
    private JCheckBox wed;

    /** JCheckBox for Thursday availability */
    private JCheckBox thu;

    /** JCheckBox for Friday availability */
    private JCheckBox fri;

    /** JCheckBox for Saturday availability */
    private JCheckBox sat;

    /** JLable to show where to enter first name */
    private JLabel firstName;

    /** JLable to show where to enter last name */
    private JLabel lastName;

    /** JLable to show where to enter username */
    private JLabel userName;

    /** JLabel asking if over 18 */
    private JLabel age;

    /** JLabel asking if US Citizen */
    private JLabel US;

    /** JLabel asking about availability */
    private JLabel availability;

    /** JFrame for appliction */
    private JFrame frame;

    /** ButtonGroup for age related radio buttons */
    private ButtonGroup ageGroup;

    /** ButtonGroup for citizenship related radio buttons */
    private ButtonGroup USGroup;

    /** JRadioButton for over 18 */
    private JRadioButton ageYes;

    /** JRadioButton for under 18 */
    private JRadioButton ageNo;

    /** JRadioButton for US citizen */
    private JRadioButton yesUS;

    /** JRadioButton for not US citizen */
    private JRadioButton noUS;

    /** JMenuItem to exit app */
    private JMenuItem exitm;

    /** JMenuItem to save applicaiton */
    private JMenuItem savem;

    /** JMenuItem to submitt applicaiton */
    private JMenuItem submitm;

    /**
     * Instantiate the GUI application
     *
     * @param args command line arguments (not used)
     */
    public static void main (String[] args) {
    
        JobApp gui = new JobApp();
        gui.start();
    
    }

    /**
     * Creates and organizes GUI
     */
    public void start() {
    
        frame = new JFrame("My Second Java GUI App");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        makeMenus();

        //This section defines all the elements on the page
        firstNameOutput = new JTextField(TEXT_LENGTH);
        lastNameOutput = new JTextField(TEXT_LENGTH);
        userNameOutput = new JTextField(TEXT_LENGTH);
        save = new JButton("Save");
        save.addActionListener(this);
        submit = new JButton("Submit");
        submit.addActionListener(this);
        exit = new JButton("Exit");
        exit.addActionListener(this);
        sun = new JCheckBox("Sunday");
        mon = new JCheckBox("Monday");
        tue = new JCheckBox("Tuesday");
        wed = new JCheckBox("Wednesday");
        thu = new JCheckBox("Thursday");
        fri = new JCheckBox("Friday");
        sat = new JCheckBox("Saturday");
        firstName = new JLabel("Enter First Name:");
        lastName = new JLabel("Enter Last Name:");
        userName = new JLabel("Enter User Name:");
        age = new JLabel("Are you 18 years old or older?");
        US = new JLabel("Are you legally able to work in the US?");
        availability = new JLabel("What days are you available for work?");
        
        //special section for check box inputs

        //special section dedicated to radio buttons
        ageYes = new JRadioButton("Yes");    
        ageNo = new JRadioButton("No");

        ageGroup = new ButtonGroup();    
          
        ageGroup.add(ageYes);
        ageGroup.add(ageNo);

        yesUS = new JRadioButton("Yes");    
        noUS = new JRadioButton("No");

        USGroup = new ButtonGroup();    
          
        USGroup.add(yesUS);
        USGroup.add(noUS);

        //special section for radio button inputs

        //This section adds all of the GUI elements    
        contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.Y_AXIS));
        contentPane.add(save);
        contentPane.add(submit);
        contentPane.add(userName);
        //userNameOutput.setPreferredSize(new Dimension (150, 25));
        contentPane.add(userNameOutput);
        contentPane.add(firstName);
        //firstNameOutput.setPreferredSize(new Dimension (150, 25));
        contentPane.add(firstNameOutput);
        contentPane.add(lastName);
        contentPane.add(lastNameOutput);
        //lastNameOutput.setPreferredSize(new Dimension (150, 25));
        contentPane.add(age);
        contentPane.add(ageYes);
        contentPane.add(ageNo);
        contentPane.add(US);
        contentPane.add(yesUS);
        contentPane.add(noUS);
        contentPane.add(availability);
        contentPane.add(sun);
        contentPane.add(mon);
        contentPane.add(tue);    
        contentPane.add(wed);
        contentPane.add(thu);
        contentPane.add(fri);
        contentPane.add(sat);
        
        frame.pack();
        //frame.setSize(500,300);
        frame.setVisible(true);
    }

    /**
     * ActionListener for GUI elements
     *
     * @param e ActionEvent to start event-driven methods
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == save || e.getSource() == savem ) {
        
            output();
    
        }
        if (e.getSource() == submit || e.getSource() == submitm) {

            output();
            JOptionPane.showMessageDialog(null, "Application Submitted Successfully!" +
                    " Thank You for your interest!");
            System.exit(0);

        }
        if (e.getSource() == exitm) {

            System.exit(0);

        }
    }

    /**
     * Grabs user entered info from GUI and outputs it to a .app file
     */
    private void output() {

        MakeFile application = new MakeFile(userNameOutput.getText() + ".app");
        application.writeFile(firstNameOutput.getText() + " " + lastNameOutput.getText());
        application.writeFile("Can work Sunday: " + ((sun.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Monday: " + ((mon.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Tuesday: " + ((tue.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Wednesday: " + ((wed.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Thursday: " + ((thu.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Friday: " + ((fri.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("Can work Saturday: " + ((sat.isSelected()) ? ("Yes") : ("No")));
        application.writeFile("18 or older: " + (ageYes.isSelected() ? "Yes" : "No"));
        application.writeFile("US Citizen: " + (yesUS.isSelected() ? "Yes" : "No"));
        application.closeFile();


    }

    /**
     * Formats menus, and menu bar
     */
    private void makeMenus(){//This section defines the menu and its items
        
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        // actionListeners for menu
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        
        menu = new JMenu("File");
        menuBar.add(menu);
        
        savem = new JMenuItem("Save");
        savem.addActionListener(this);
        menu.add(savem);
        submitm = new JMenuItem("Submit");
        submitm.addActionListener(this);
        menu.add(submitm);
        menu.addSeparator();
        exitm = new JMenuItem("Exit");
        exitm.addActionListener(this);
        menu.add(exitm);
    
    }

}    

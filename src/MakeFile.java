import java.io.*;

/**
* Easy way to create and test files
*
* @author Scott Hasserd
* @version 1
*/
public class MakeFile {

    /** PrintWriter object */
    private PrintWriter outFile = null;

    /**
    * Makes file using given string 
    *
    * @param fileName file to be created/written to
    */
    public MakeFile(String fileName) {
    
        try {

            outFile = new PrintWriter(fileName);
        
        } catch (FileNotFoundException e) {

            System.out.println("File " + fileName +
                    " not found or inaccessable");
            System.exit(1);

        }

    }
 
    /**
    * Method write to file
    *
    * @param line line to be printed to out file
    */
    public void writeFile(String line) {
       
        outFile.println(line);
        
    }
 
    /**
    * Method close file
    */
    public void closeFile() {

        outFile.close();

    }
}
